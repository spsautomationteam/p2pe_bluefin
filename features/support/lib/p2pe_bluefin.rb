class P2peBluefin

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"


  #@base_url
  @@reference_num = '0'
  @@content_type =''
  @authorization=''
  @auth=''
  @response=''
  @parsed_response=''
  @url=''

  #constructor,Reading yaml file
  def initialize()
    config = YAML.load_file('config.yaml')
    @bluefin_url = config['bluefin_url']
    @auth = config['authorization']
    @bankcard_url =config['bankcard_url']
    puts "@@bankcard_url :#{@bankcard_url}, @bluefin_url: #{@bluefin_url}, @auth:#{@auth}"
    set_url 'bluefin'
  end


  def set_url(value)
    case value
      when 'bankcard'
        @url = @bankcard_url
      when 'bluefin'
        @url = @bluefin_url
    end

  end

  #set header
  def set_header(header, value)
    puts "header : #{header}, value : #{value}"
    case header
      when "content-type"
        @@content_type =value
      when "Authorization"
        if (value.eql? 'Authorization')
          @authorization =@auth
        else
          @authorization =value
        end
    end
  end

  #set json body
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

  #set post
  def post(path)
    post_ref path, ''
  end

  def post_existing_refnum(path)
    post_ref path, @@reference_num
  end

  def patch_existing_refnum(path)
    patch_ref path, @@reference_num
  end

  #post method
  def post_ref(path, ref_num)
    full_url= @url+path

    if (!ref_num.empty?)
      full_url +='/'+ref_num
    end

    #Debug urls
    puts "path: #{path}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@@content_type: #{@@content_type}"

    #Do post action and get response
    begin
      @response = RestClient.post full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
      @@reference_num = JSON.parse(@response.body)['reference']
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end


  end


  def patch(path)
    patch_ref path, ''
  end

  #patch method
  def patch_ref(path, ref_num)

    full_url= @url+path
    if (!ref_num.empty?)
      full_url +='/'+ref_num
      puts "ref_num:#{ref_num}"
    end

    puts "#patch_ref "
    #Debug urls
    puts "path#{path}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@@content_type: #{@@content_type}"


    #Do post action and get response
    begin
      @response = RestClient.patch full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end


    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

  #verify response empty
  def verify_response_empty()
    expect(@response.empty?).to be_truthy, "Expected : empty response, got : #{@response}"
  end

  #verify response contain string
  def verify_response_params_contain(response_param, value)
    if @parsed_response[response_param].nil?
      expect(false).to be_truthy, "Expected : #{value} ,got : nil } \nResponesbody:#{@parsed_response}"
    else
      expect(@parsed_response[response_param].to_s.delete('').include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
    end
  end

  def verify_response_empty
    expect(@response.empty?).to be_truthy, "Expected : Response  empty ,got : #{@response} \nResponesbody:#{@parsed_response}"
  end

  def verify_response_params(response_param, value)

    if (response_param == "code")
      expect(@response.code.to_s).to eql(value), "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
    else
      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
      else
        expect(@parsed_response[response_param].to_s.delete('')).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
      end

    end
  end

  #verify resposne header
  def verify_response_headers(header_param, value)
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end


end