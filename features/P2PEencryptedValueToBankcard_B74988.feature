@intg
Feature:Verify   transaction for emv, swipe, fallback1, fallback2 .

  @card_data_transaction
  Scenario Outline: Verify swipe data transaction
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to <json>
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header Content-Type should be application/json
    And response body path status should be Approved
    Examples:
      | type      | json                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | swipe     | {"TransactionId":"048f3a12372347d7bace8ffc62a8192c", "Retail":{"Amounts":{"Total":7.11,"Tax":2.0,"Shipping":0.0}, "TrackData":{"Value":"37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373=378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true, "Emv":{"Tags":"57:37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7\|","SerialNumber":"82377071","KernelVersion":"0467"}, "OrderNumber":"701204767"}, "TransactionCode":"Sale" }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
      | emv       | {"TransactionId":"048f3a12372347d7bace8ffc62a8192c","Retail":{"Amounts":{"Total":7.22,"Tax":2.0,"Shipping":0.0},"TrackData":{"Value":"2223000000002020=191220100000000000000","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true,"Emv":{"Tags":"4F:a0000000041010\|50:MASTERCARD\|57:2223000000002020d191220100000000000000\|82:3800\|84:a0000000041010\|95:0000088000\|9A:171031\|9B:e800\|9C:00\|1005:3030\|100E:454e\|5F2A:0840\|5F34:00\|9000:42\|9001:43\|9F02:000000000720\|9F03:000000000000\|9F06:a0000000041010\|9F09:0002\|9F10:0014a08003222000ae2200000000096000ff\|9F11:01\|9F1A:0840\|9F1B:00000000\|9F1E:3832333737303731\|9F21:232111\|9F26:c5e9b0f472efa83a\|9F27:80\|9F33:e0f8c8\|9F34:1e0300\|9F35:22\|9F36:0042\|9F37:8623e044\|9F39:05\|9F40:4000f09001\|9F41:00000034\|9F53:52\|FF1F:323232333030323032303136313139313232303130303031423030303232383030303030303030343030303833303131343337343334303736383337343432313735313d363937363936303839363239353736393638383833323938424638434235464435464131324546374341373131423941393637413945393630414244423933394538313738384341434542343543333233344431464642313635383138434636354531303930463139353244413634373139314433423835433141334441363130374434353536324238393342373031364336313242434436\|","SerialNumber":"82377071","KernelVersion":"0467"},"OrderNumber":"701204767"},"TransactionCode":1} |
      | fallback1 | {"TransactionId":"aee7fc502fee46b6b2861f5d190c68d0","Retail":{"Amounts":{"Total":3.33,"Tax":3.0,"Shipping":0.0},"TrackData":{"Value":"37144984311511812101817CHASE PAYMENTECH 1B0002280000000040006E011436319932734780679=8742561244699210254732498F02A11169872D96ADF73C4802E4B396397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true,"Emv":{"Tags":"57:37144984311511812101817CHASE PAYMENTECH 1B0002280000000040006E011436319932734780679D8742561244699210254732498F02A11169872D96ADF73C4802E4B396397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7\|","SerialNumber":"82377071","KernelVersion":"0467"},"OrderNumber":"400261923"},"TransactionCode":1}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
      | fallback2 | {"TransactionId":"048f3a12372347d7bace8ffc62a8192c","Retail":{"Amounts":{"Total":7.44,"Tax":2.0,"Shipping":0.0},"TrackData":{"Value":"541333043416118122011001B000228000000004000860114349895338966960554=38044320611576606321AED89F21E2F4C7FBDAE434EA8C637849690968AA3FB8321B50A2DEE42CBFF4BC643D7C9305C51399D2985FEB01D3D89EA954FB7F9EC7286AB5C6D86577F6C0DF50","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true,"Emv":{"Tags":"57:541333043416118122011001B000228000000004000860114349895338966960554D38044320611576606321AED89F21E2F4C7FBDAE434EA8C637849690968AA3FB8321B50A2DEE42CBFF4BC643D7C9305C51399D2985FEB01D3D89EA954FB7F9EC7286AB5C6D86577F6C0DF50\|","SerialNumber":"82377071","KernelVersion":"0467","Fallback":0},"OrderNumber":"701204767"},"TransactionCode":1}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |


  @card-data-with-amount-zero
  Scenario: Verify fallback data2 transaction
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {"TransactionId":"048f3a12372347d7bace8ffc62a8192c","Retail":{"Amounts":{"Total":0,"Tax":2.0,"Shipping":0.0},"TrackData":{"Value":"541333043416118122011001B000228000000004000860114349895338966960554=38044320611576606321AED89F21E2F4C7FBDAE434EA8C637849690968AA3FB8321B50A2DEE42CBFF4BC643D7C9305C51399D2985FEB01D3D89EA954FB7F9EC7286AB5C6D86577F6C0DF50","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true,"Emv":{"Tags":"57:541333043416118122011001B000228000000004000860114349895338966960554D38044320611576606321AED89F21E2F4C7FBDAE434EA8C637849690968AA3FB8321B50A2DEE42CBFF4BC643D7C9305C51399D2985FEB01D3D89EA954FB7F9EC7286AB5C6D86577F6C0DF50|","SerialNumber":"82377071","KernelVersion":"0467","Fallback":0},"OrderNumber":"701204767"},"TransactionCode":1}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    And response header Content-Type should be application/json
    And response body path errorCode should be InvalidRequestData
    And response body path errorDescription should contain request.Retail.Amounts: Invalid Total Amount


  @card-data-with-wrong-txn-code
  Scenario: Verify fallback data2 transaction
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {"TransactionId":"048f3a12372347d7bace8ffc62a8192c", "Retail":{"Amounts":{"Total":7.55,"Tax":2.0,"Shipping":0.0}, "TrackData":{"Value":"37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373=378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true, "Emv":{"Tags":"57:37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|","SerialNumber":"82377071","KernelVersion":"0467"}, "OrderNumber":"701204767"}, "TransactionCode":"xx" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    And response header Content-Type should be application/json
    And response body path errorCode should be InvalidRequestData
    And response body path errorDescription should contain request: The TransactionCode field is required

  @card-data-with-invalid-track-data
  Scenario: Verify fallback data2 transaction
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {"TransactionId":"048f3a12372347d7bace8ffc62a8192c", "Retail":{"Amounts":{"Total":7.66,"Tax":2.0,"Shipping":0.0}, "TrackData":{"Value":"ABC 37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373=378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true, "Emv":{"Tags":"57:37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|","SerialNumber":"82377071","KernelVersion":"0467"}, "OrderNumber":"701204767"}, "TransactionCode":"Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    And response header Content-Type should be application/json
    And response body path errorCode should be TransactionRejected
    And response body path errorDescription should be INVALID C_CARDNUMBER

  @card-data-with-invalid-emv-tags
  Scenario: Verify fallback data2 transaction
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {"TransactionId":"048f3a12372347d7bace8ffc62a8192c", "Retail":{"Amounts":{"Total":7.77,"Tax":2.0,"Shipping":0.0}, "TrackData":{"Value":"37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373=378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true, "Emv":{"Tags":"57:XXX+   37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|","SerialNumber":"82377071","KernelVersion":"0467"}, "OrderNumber":"701204767"}, "TransactionCode":"Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    And response header Content-Type should be application/json
    And response body path errorCode should be TransactionRejected
    And response body path errorDescription should be INVALID C_CARDNUMBER

  @card-data-with-empty-json
  Scenario: Verify fallback data2 transaction
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    And response header Content-Type should be application/json
    And response body path errorCode should be InvalidRequestData
    And response body path errorDescription should be request: The TransactionCode field is required.

  @card-data-with-invalid-string-as-json
  Scenario: Verify f transaction with invalid string
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to abc
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    And response body path message should be No request content was found

  @card-data-with-empty-emv-tag
  Scenario: Verify transaction with empty emv tag
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {"TransactionId":"048f3a12372347d7bace8ffc62a8192c", "Retail":{"Amounts":{"Total":7.88,"Tax":2.0,"Shipping":0.0}, "TrackData":{"Value":"37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373=378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7","Format":0,"IsContactless":false},"DeviceId":"CardPresent.TestClient","CardPresent":true, "Emv":{}, "OrderNumber":"701204767"}, "TransactionCode":"Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    Then response body path errorCode should be InvalidRequestData
    Then response body path errorDescription should contain request.Retail.Emv: Tags is required; request.Retail.Emv: SerialNumber is required

  @card-data-with-empty-track-data
  Scenario: Verify transaction with empty track data
    Given I set Authorization header to Authorization
    And I set url to bankcard
    And I set content-type header to application/json
    And I set body to {"TransactionId":"048f3a12372347d7bace8ffc62a8192c", "Retail":{"Amounts":{"Total":7.99,"Tax":2.0,"Shipping":0.0}, "TrackData":{},"DeviceId":"CardPresent.TestClient","CardPresent":true, "Emv":{"Tags":"57:37144984311511812101817CHASE PAYMENTECH 1B00022800000000400085011436721368493893373D378450989952364941363266E5A4D67D738391B0E998F6BDDF21FC96397B0E892CB4E8DC91CE555D8D12B27E4DAEABF2767BF65891B1A49A87A9320DD20F1F1B1825506247D2CBF172ED2AFA7|","SerialNumber":"82377071","KernelVersion":"0467"}, "OrderNumber":"701204767"}, "TransactionCode":"Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 400
    Then response body path errorCode should be InvalidRequestData
    Then response body path errorDescription should contain request.Retail.TrackData: The TrackData.Value field is required; request.Retail.TrackData: The TrackData.Format field is required
